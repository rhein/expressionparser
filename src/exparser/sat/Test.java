package exparser.sat;
import java.io.File;
import java.io.FileReader;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.constraints.Constraint;
import choco.kernel.solver.Solver;

public class Test {		
	public static void main(String[] args) throws Exception {
		File ExparserSatFile = new File("./Formula.txt");
		ExparserSatParser p = new ExparserSatParser(new ExparserSatScanner(new FileReader(ExparserSatFile)));
		
		Constraint modelConstraints;
		// parsing happens here
		System.out.println("parsing " + ExparserSatFile.getAbsolutePath());
		modelConstraints = (Constraint) p.parse().value;

		// evaluation
		Model m = new CPModel();
		m.addConstraint(modelConstraints);
		Solver solver;
		solver = new CPSolver();
		solver.read(m);
		/*
		solver.solve();
		if (solver.isFeasible()) {
			System.out.println("feasible");
			System.out.println(solver.solutionToString());
		} else {
			System.out.println("not feasible");
		} */

		solver.solveAll();
		if (! solver.isFeasible()) {
			System.out.println("infeasible");
		} else {
			System.out.println(solver.getSolutionCount() + " solutions:");
			solver = new CPSolver();
			solver.read(m);
			if(solver.solve()){
				do{
					System.out.println(solver.solutionToString());
				}while(solver.nextSolution());
			}
			
		}
	}
}
