package exparser.bdd;
import java_cup.runtime.Symbol;

@SuppressWarnings(value = { "all" })
%%

%cup
%class ExparserBddScanner
%implements ExparserBddSym
%line
%column

%{
  private Symbol symbol(String name, int sym, String val) {
    return new Symbol(sym, yyline, yycolumn, val);
  }
  private Symbol symbol(String name, int type) {
    return new Symbol(type, yyline, yycolumn);
  }
  
  private void error(String message) {
    throw new RuntimeException(message + " near " + yyline +":" + yycolumn);
  }
%}
%eofval{
    return symbol("EOF", ExparserBddSym.EOF);
%eofval}

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n] | .
WhiteSpace     = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}

Identifier = [:jletter:] [:jletterdigit:]*


%%

/* keywords */
<YYINITIAL> "!"                { return symbol("not", ExparserBddSym.NOT); }
<YYINITIAL> "&"                { return symbol("and", ExparserBddSym.AND); }
<YYINITIAL> "|"                 { return symbol("or", ExparserBddSym.OR); }

/* I know that && is lazy, but for the sake of simplicity its the same here. */
<YYINITIAL> "&&"                { return symbol("and", ExparserBddSym.AND); }
<YYINITIAL> "||"                 { return symbol("or", ExparserBddSym.OR); }

<YYINITIAL> "not"                { return symbol("not", ExparserBddSym.NOT); }
<YYINITIAL> "and"                { return symbol("and", ExparserBddSym.AND); }
<YYINITIAL> "or"                 { return symbol("or", ExparserBddSym.OR); }

<YYINITIAL> "=>"                 { return symbol("implies", ExparserBddSym.IMPLIES); }
<YYINITIAL> "<="                 { return symbol("implies_reversed", ExparserBddSym.IMPLIESREV); }
<YYINITIAL> "<=>"                 { return symbol("equiv", ExparserBddSym.EQUIV); }


<YYINITIAL> "oneOf"                { return symbol("oneOf", ExparserBddSym.ONE_OF); }
<YYINITIAL> "noneOf"                { return symbol("noneOf", ExparserBddSym.NONE_OF); }
<YYINITIAL> "someOf"                 { return symbol("someOf", ExparserBddSym.SOME_OF); }
<YYINITIAL> "oneOfOrNone"                 { return symbol("oneOfOrNone", ExparserBddSym.ONE_OF_OR_NONE); }

<YYINITIAL> "def"                 { return symbol("defined", ExparserBddSym.DEFINED); }
<YYINITIAL> "definedEx"                 { return symbol("defined", ExparserBddSym.DEFINED); }
<YYINITIAL> "defined"                 { return symbol("defined", ExparserBddSym.DEFINED); }

/* true/false */
<YYINITIAL> "TRUE"                           { return symbol("TRUE", ExparserBddSym.TRUE); }
<YYINITIAL> "FALSE"                           { return symbol("FALSE", ExparserBddSym.FALSE); }
<YYINITIAL> "True"                           { return symbol("TRUE", ExparserBddSym.TRUE); }
<YYINITIAL> "False"                           { return symbol("FALSE", ExparserBddSym.FALSE); }

<YYINITIAL> {
  /* identifiers */ 
  {Identifier}                   { return symbol("ID", ExparserBddSym.IDENTIFIER, yytext()); }
 
  /* literals */

  /* operators */
  "("                           { return symbol("(", ExparserBddSym.OBRACKETS); }
  ")"                           { return symbol(")", ExparserBddSym.CBRACKETS); }
  
  ","                           { return symbol("(", ExparserBddSym.COMMA); }


  /* comments */
  {Comment}                      { /* ignore */ }
 
  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }
}

<<EOF>> {return symbol("EOF", ExparserBddSym.EOF); }
/* error fallback */
.|\n                             { error("Illegal character <"+yytext()+">"); }
