package exparser.bdd.tools;

import static org.junit.Assert.*;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;

import net.sf.javabdd.BDD;

import org.junit.Before;
import org.junit.Test;

import exparser.bdd.ExparserBddParser;
import exparser.bdd.ScannerCreator;
import exparser.bdd.tools.BDDUtils.BDDAllsatIterator;
import exparser.bdd.tools.BDDUtils.BDDCNFIterator;

public class BDDUtils_test {

	@Before
	public void setUp() throws Exception {
		//BddManager.resetStatics();
	}

	@Test
	public void test_CNF_iterator1() throws Exception {
		String expression = "A and (!B or C)";
		// test operator precedence
		test_CNF_generation(expression);
	}
	@Test
	public void test_CNF_iterator2() throws Exception {
		String expression = "A or (!B and C)";
		// test operator precedence
		test_CNF_generation(expression);
	}
	@Test
	public void test_CNF_iterator3() throws Exception {
		String expression = "((wet && cold) || (poor && hungry))";
		// test operator precedence
		test_CNF_generation(expression);
	}
	@Test
	public void test_CNF_iterator4() throws Exception {
		String expression = "A <=> B";
		// test operator precedence
		test_CNF_generation(expression);
	}
	
	/**
	 * We parse the string, generate a CNF from it, parse the CNF, and test whether it is equal to the originally parsed String.
	 * @throws Exception
	 */
	private void test_CNF_generation(String expression) throws Exception {
		ExparserBddParser p = new ExparserBddParser(ScannerCreator.createScanner(new StringReader(
				expression)));
		BDD model_orig = (BDD) p.parse().value;
		BDDCNFIterator iter = new BDDCNFIterator(model_orig);
		Collection<BDD> varList = BddManager.varToBDD.values();
		Collection<String> clauses_str = new ArrayList<String>();
		for (Object sol : iter) {
			byte[] clause = (byte[]) sol;
			if (clause!=null) {
				ArrayList<String> clauseString_list = new ArrayList<String>();
				for (BDD varBDD : varList) {
					if (clause[varBDD.var()] == 1)
						clauseString_list.add(BddManager.BDDToString(varBDD, false, false));
					else if (clause[varBDD.var()] == 0)
						clauseString_list.add("!" + BddManager.BDDToString(varBDD, false, false));
					//else if (solution[varBDD.var()] == -1)
					//	clauseString_list.add("[" + BddManager.BDDToString(varBDD, false, false) + "]");
				}
				clauses_str.add(BDDUtils.arrayToString(clauseString_list.toArray()).replace(",", " or "));
			}
		}
		String cnf = BDDUtils.arrayToString(clauses_str.toArray()).replace(",", " and\n ");
		System.out.println(cnf);
		// parse cnf and test for equality
		p = new ExparserBddParser(ScannerCreator.createScanner(new StringReader(cnf)));
		BDD model_cnf = (BDD) p.parse().value;
		if (model_orig.equals(model_cnf)) {
			System.out.println("CNF generation correct!");
		} else {
			System.out.println("CNF generation failed!");
			fail();
		}
	}

}
