#!/bin/sh

#directory for class files
mkdir -p bin

echo "Expression Parser BDD"
ant -f src/exparser/bdd/build.xml

echo "compile"
javac -d bin/ -cp ./src/:./lib/java-cup-11a.jar:./lib/javabdd-1.0b2.jar ./src/exparser/bdd/Test.java

echo "call bdd test"
java -cp ./bin/:./lib/java-cup-11a.jar:./lib/javabdd-1.0b2.jar exparser.bdd.Test

echo "------ BDD done"

echo "Expression Parser SAT"
ant -f src/exparser/sat/build.xml

echo "compile"
javac -d bin/ -cp ./src/:./lib/java-cup-11a.jar:./lib/choco-solver-2.1.1-20101108.160543-4.jar ./src/exparser/sat/Test.java

echo "call sat test (should have 3 solutions)"
java -cp ./bin/:./lib/java-cup-11a.jar:./lib/choco-solver-2.1.1-20101108.160543-4.jar exparser.sat.Test



echo "------ SAT done"